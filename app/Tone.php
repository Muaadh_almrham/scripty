<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Project;

class Tone extends Model
{
    /**
     * The attributes that shoud not be mass assignable.
     *
     * @var array
    */
    protected $guarded  = [];    
    
    /**
     * The table that corresponds to this model
     *
     * @var string
    */
    protected $table  = "tones";

    /**
     * Define Tone Project relationship
     * A Project can have Many tones
     *  and 
     * A Tone can belong to many Projects as well
     * @var collectoin
    */
    public function projects() {
        return $this->belongsToMany(Project::class, 'project_tone', 'tone_id', 'project_id');
    }
}

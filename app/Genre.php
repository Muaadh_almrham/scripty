<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Project;

class Genre extends Model
{
    /**
     * The attributes that shoud not be mass assignable.
     *
     * @var array
    */
    protected $guarded  = [];   
    
    /**
     * The table that corresponds to this model
     *
     * @var string
    */
    protected $table  = "genres";

    /**
     * Define Genre Project relationship
     * A Project can have Many Genres
     *  and 
     * A Genre can belong to many Projects
     * @var collectoin
    */
    public function projects() {
        return $this->belongsToMany(Project::class, 'genre_project', 'genre_id', 'project_id');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Equipment extends Model
{
    /**
     * The attributes that shoud not be mass assignable.
     *
     * @var array
    */
    protected $guarded  = [];    
    
    /**
     * The table that corresponds to this model
     *
     * @var string
    */
    protected $table  = "equipment";

    /**
     * Define Equipment Project relationship
     * A Project can have Many Equipments
     *  and 
     * An Equipment can belong to many Projects as well
     * @var collectoin
    */
    public function projects() {
        return $this->belongsToMany(Project::class, 'equipment_project', 'equipment_id', 'project_id');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Project;

class Industry extends Model
{
    /**
     * The attributes that shoud not be mass assignable.
     *
     * @var array
    */
    protected $guarded  = [];
    
    /**
     * The table that corresponds to this model
     *
     * @var string
    */
    protected $table  = "industries";

    /**
     * Define Industry Project relationship
     * A Project belongs to only one Industry
     *  While 
     * An Industry contains many Projects
     * @var object
    */
    public function projects() {
        return $this->hasMany(Project::class);
    }
}

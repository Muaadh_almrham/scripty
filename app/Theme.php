<?php

namespace App;
use App\Project;
use Illuminate\Database\Eloquent\Model;

class Theme extends Model
{
        /**
     * The attributes that shoud not be mass assignable.
     *
     * @var array
    */
    protected $guarded  = [];
    
    /**
     * The table that corresponds to this model
     *
     * @var string
    */
    protected $table  = "themes";

    /**
     * Define Theme Project relationship
     * A Project has Many Themes
     *  While 
     * A Theme owns many Projects
     * @var object
    */
    public function projects() {
        return $this->hasMany(Project::class);
    }
}

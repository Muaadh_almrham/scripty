<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\Project;
use App\Position;
class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * Define User Project relationship
     * A Project belongs to only one user
     *  While 
     * A User owns many Projects
     * @var object
    */
    public function projects(){
        $this->hasMany(Project::class);
    }

    /**
     * Define User Position relationship
     * A User has only one position
     *  While 
     * A Position has many Users
     * @var object
    */
    public function position(){
        $this->belongsTo(Position::class);
    }
}

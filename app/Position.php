<?php

namespace App;
use App\User;
use Illuminate\Database\Eloquent\Model;

class Position extends Model
{
     /**
     * The attributes that shoud not be mass assignable.
     *
     * @var array
    */
    protected $guarded  = [];    
    
    /**
     * The table that corresponds to this model
     *
     * @var string
    */
    protected $table  = "positions";

    /**
     * Define User Project relationship
     * A Project belongs to only one user
     *  While 
     * A User owns many Projects
     * @var object
    */
    public function users() {
        return $this->hasMany(User::class, 'user_id');
    }
}

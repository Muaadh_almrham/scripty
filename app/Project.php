<?php

namespace App;
use App\User;
use App\Industry;
use App\Theme;
use App\Genre;
use App\Tone;
use App\Equipment;
use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    /**
     * The attributes that shoud not be mass assignable.
     *
     * @var array
    */
    protected $guarded  = [];    
    
    /**
     * The table that corresponds to this model
     *
     * @var string
    */
    protected $table  = "projects";

    /**
     * Define User Project relationship
     * A Project belongs to only one user
     *  While 
     * A User owns many Projects
     * @var object
    */
    public function user() {
        return $this->belongsTo(User::class, 'user_id');
    }

    /**
     * Define Industry Project relationship
     * A Project belongs to only one Industry
     *  While 
     * An Industry contains many Projects
     * @var object
    */
    public function industry() {
        return $this->belongsTo(Industry::class, 'industry_id');
    }

    /**
     * Define Theme Project relationship
     * A Project has Many Themes
     *  While 
     * A Theme owns many Projects
     * @var object
    */
    public function theme() {
        return $this->belongsTo(Theme::class, 'theme_id');
    }

    /**
     * Define Genre Project relationship
     * A Project can have Many Genres
     *  and 
     * A Genre can belong to many Projects
     * @var collectoin
    */
    public function genres() {
        return $this->belongsToMany(Genre::class, 'genre_project', 'project_id', 'genre_id');
    }

    /**
     * Define Tone Project relationship
     * A Project can have Many tones
     *  and 
     * A Tone can belong to many Projects as well
     * @var collectoin
    */
    public function tones() {
        return $this->belongsToMany(Tone::class, 'project_tone', 'project_id', 'tone_id');
    }

    /**
     * Define Equipment Project relationship
     * A Project can have Many Equipments
     *  and 
     * An Equipment can belong to many Projects as well
     * @var collectoin
    */
    public function equipments() {
        return $this->belongsToMany(Equipment::class, 'equipment_project', 'project_id', 'equipment_id');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projects', function (Blueprint $table) {
            $table->id();
            $table->string('title');            
            $table->longText('idea');            
            $table->longText('synopsis');            
            $table->string('genre');            
            $table->string('tone');            
            $table->longText('dimension');            
            $table->longText('values');            
            $table->longText('added_values');            
            $table->json('audience');            
            $table->foreignId('user_id')->constrained();            
            $table->foreignId('industry_id')->constrained();            
            $table->foreignId('theme_id')->constrained();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('projects');
    }
}

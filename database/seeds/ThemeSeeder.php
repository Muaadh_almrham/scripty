<?php

use App\Theme;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;


class ThemeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // static data for Themes
        $data = [
            ['name'=>'War and Crises'   , 'created_at'=> Carbon::now()->format('Y-m-d H:i:s')],
            ['name'=>'Refugees'         , 'created_at'=> Carbon::now()->format('Y-m-d H:i:s')],
            ['name'=>'Environmental'    , 'created_at'=> Carbon::now()->format('Y-m-d H:i:s')],
            ['name'=>'Social'           , 'created_at'=> Carbon::now()->format('Y-m-d H:i:s')],
            ['name'=>'Polital'          , 'created_at'=> Carbon::now()->format('Y-m-d H:i:s')],
            ['name'=>'Cause'            , 'created_at'=> Carbon::now()->format('Y-m-d H:i:s')],
            ['name'=>'Art'              , 'created_at'=> Carbon::now()->format('Y-m-d H:i:s')],
            ['name'=>'Economy'          , 'created_at'=> Carbon::now()->format('Y-m-d H:i:s')],
            ['name'=>'Business'         , 'created_at'=> Carbon::now()->format('Y-m-d H:i:s')],
            ['name'=>'Culture'          , 'created_at'=> Carbon::now()->format('Y-m-d H:i:s')],
            ['name'=>'Science'          , 'created_at'=> Carbon::now()->format('Y-m-d H:i:s')],
            ['name'=>'Linguistic'       , 'created_at'=> Carbon::now()->format('Y-m-d H:i:s')],
 ];

        // insert Themes data
        Theme::insert($data);
    }
}

<?php

use App\Industry;
use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;


class IndustySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // static data for Industries
        $data = [
            ['name'=>'News' , 'created_at'=> Carbon::now()->format('Y-m-d H:i:s')],
            ['name'=>'Sport News' , 'created_at'=> Carbon::now()->format('Y-m-d H:i:s')],
            ['name'=>'Social media' , 'created_at'=> Carbon::now()->format('Y-m-d H:i:s')],
            ['name'=>'Film' , 'created_at'=> Carbon::now()->format('Y-m-d H:i:s')],
            ['name'=>'Drama' , 'created_at'=> Carbon::now()->format('Y-m-d H:i:s')],
            ['name'=>'Animation' , 'created_at'=> Carbon::now()->format('Y-m-d H:i:s')],
            ['name'=>'Advertising' , 'created_at'=> Carbon::now()->format('Y-m-d H:i:s')],
            ['name'=>'Theatre' , 'created_at'=> Carbon::now()->format('Y-m-d H:i:s')],
            ['name'=>'Cartoon ' , 'created_at'=> Carbon::now()->format('Y-m-d H:i:s')],
            ['name'=>'Interactive Stories' , 'created_at'=> Carbon::now()->format('Y-m-d H:i:s')],
            ['name'=>'Digital' , 'created_at'=> Carbon::now()->format('Y-m-d H:i:s')],
            ['name'=>'Podcasts' , 'created_at'=> Carbon::now()->format('Y-m-d H:i:s')],
        ];

        // insert Industries data
        Industry::insert($data);
    }
}

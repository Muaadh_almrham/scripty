<?php

use App\Genre;
use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;


class GenreSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // static data for genres
        $data = [
            ['name'=>'Action' , 'created_at'=> Carbon::now()->format('Y-m-d H:i:s')],
            ['name'=>'Comedy' , 'created_at'=> Carbon::now()->format('Y-m-d H:i:s')],
            ['name'=>'Adventure' , 'created_at'=> Carbon::now()->format('Y-m-d H:i:s')],
            ['name'=>'Police' , 'created_at'=> Carbon::now()->format('Y-m-d H:i:s')],
            ['name'=>'Drama' , 'created_at'=> Carbon::now()->format('Y-m-d H:i:s')],
            ['name'=>'Epic/Historical' , 'created_at'=> Carbon::now()->format('Y-m-d H:i:s')],
            ['name'=>'Thriller' , 'created_at'=> Carbon::now()->format('Y-m-d H:i:s')],
            ['name'=>'Musical' , 'created_at'=> Carbon::now()->format('Y-m-d H:i:s')],
            ['name'=>'Science Fiction' , 'created_at'=> Carbon::now()->format('Y-m-d H:i:s')],
            ['name'=>'War' , 'created_at'=> Carbon::now()->format('Y-m-d H:i:s')],
            ['name'=>'Romance' , 'created_at'=> Carbon::now()->format('Y-m-d H:i:s')],
            ['name'=>'True Story' , 'created_at'=> Carbon::now()->format('Y-m-d H:i:s')],
            ['name'=>'Political' , 'created_at'=> Carbon::now()->format('Y-m-d H:i:s')],
        ];

        // insert genres data
        Genre::insert($data);
    }
}
